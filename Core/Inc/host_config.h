/**
 ******************************************************************************
 * @file    host_config.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    14-June-2023
 * @brief   Host UART configuration file.
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef HOST_CONFIG_H
#define HOST_CONFIG_H

/* Includes ------------------------------------------------------------------*/

#include "stm32l4xx.h"

/* Exported preprocessor constants -------------------------------------------*/

#define UNCONNECTED_PIN              {NULL}

#define HOST_TX                      {GPIOA, GPIO_PIN_2}
#define HOST_RX                      {GPIOA, GPIO_PIN_3}

#define HOST_UART_NUMBER             2
#define HOST_UART                    USART2
#define HOST_UART_IRQn               USART2_IRQn
#define HOST_UART_IRQHandler         USART2_IRQHandler
#define HOST_UART_CLK_ENABLE         __HAL_RCC_USART2_CLK_ENABLE
#define HOST_UART_AF                 GPIO_AF7_USART2
#define HOST_UART_HWCONTROL          UART_HWCONTROL_NONE

#define HOST_UART_DMA_TX_CHANNEL     DMA1_Channel7
#define HOST_UART_DMA_RX_CHANNEL     DMA1_Channel6
#define HOST_UART_DMA_TX_IRQn        DMA1_Channel7_IRQn
#define HOST_UART_DMA_RX_IRQn        DMA1_Channel6_IRQn
#define HOST_UART_DMA_TX_IRQHandler  DMA1_Channel7_IRQHandler
#define HOST_UART_DMA_RX_IRQHandler  DMA1_Channel6_IRQHandler
#define HOST_UART_DMA_CLK_ENABLE     __HAL_RCC_DMA1_CLK_ENABLE
#define HOST_UART_DMA_REQUEST        DMA_REQUEST_2

#endif
