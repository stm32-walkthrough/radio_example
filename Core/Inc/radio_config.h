/**
 ******************************************************************************
 * @file    radio_config.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    14-June-2023
 * @brief   Radio configuration file.
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef RADIO_CONFIG_H
#define RADIO_CONFIG_H

/* Includes ------------------------------------------------------------------*/

#include <stdint.h>

#include "stm32l4xx.h"

/* Exported preprocessor constants -------------------------------------------*/

// API configuration
//#define ISM_RAW_STAT ///< Uncomment to use raw statistics instead of a struct

// Protocol configuration
#define ISM_DATA_SLOT_COUNT          9
#define ISM_RETRY_COUNT              2
#define ISM_SYNC_RX_INTERVAL         1
#define ISM_SYNC_RX_MISS_MAX         10
#define ISM_SCAN_FIRST_ON_DURATION   402 ///< milliseconds, max 65535
#define ISM_SCAN_ON_DURATION         202 ///< milliseconds, max 65535
#define ISM_SCAN_OFF_DURATION        10000 ///< milliseconds, max 16777215
#define ISM_PATTERN                  0x3D, 0x47, 0x78, 0x34 ///< Must be 4 bytes separated by comma

// Hardware configuration
#define UNCONNECTED_PIN              {NULL}

#define ISM_TX                       {GPIOA, GPIO_PIN_0}
#define ISM_RX                       {GPIOA, GPIO_PIN_1}
#define ISM_CTS                      UNCONNECTED_PIN
#define ISM_RTS                      UNCONNECTED_PIN
#define ISM_GPIO0                    {GPIOA, GPIO_PIN_5}
#define ISM_GPIO1                    {GPIOA, GPIO_PIN_6}
#define ISM_GPIO2                    {GPIOA, GPIO_PIN_7}
#define ISM_GPIO3                    UNCONNECTED_PIN
#define ISM_BOOT0                    UNCONNECTED_PIN
#define ISM_NRESET                   {GPIOC, GPIO_PIN_11}
#define ISM_NPEN                     UNCONNECTED_PIN

#define ISM_GPIO1_EXTI_IRQn          EXTI9_5_IRQn
#define ISM_GPIO1_IRQHandler         EXTI9_5_IRQHandler

#define ISM_UART_NUMBER              4
#define ISM_UART                     UART4
#define ISM_UART_IRQn                UART4_IRQn
#define ISM_UART_IRQHandler          UART4_IRQHandler
#define ISM_UART_CLK_ENABLE          __HAL_RCC_UART4_CLK_ENABLE
#define ISM_UART_AF                  GPIO_AF8_UART4
#define ISM_UART_HWCONTROL           UART_HWCONTROL_NONE

#define ISM_UART_DMA_TX_CHANNEL      DMA2_Channel3
#define ISM_UART_DMA_RX_CHANNEL      DMA2_Channel5
#define ISM_UART_DMA_TX_IRQn         DMA2_Channel3_IRQn
#define ISM_UART_DMA_RX_IRQn         DMA2_Channel5_IRQn
#define ISM_UART_DMA_TX_IRQHandler   DMA2_Channel3_IRQHandler
#define ISM_UART_DMA_RX_IRQHandler   DMA2_Channel5_IRQHandler
#define ISM_UART_DMA_CLK_ENABLE      __HAL_RCC_DMA2_CLK_ENABLE
#define ISM_UART_DMA_REQUEST         DMA_REQUEST_2

#define ISM_UART_BAUDRATE            115200 ///< must be lower than 30000 for using stop2 mode of RM1

#endif
