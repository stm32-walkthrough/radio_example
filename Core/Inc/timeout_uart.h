/**
 ******************************************************************************
 * @file    timeout_uart.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    07-August-2018
 * @brief   Driver for UART using circular buffer and a read timeout
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef TIMEOUT_UART_H
#define TIMEOUT_UART_H

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdint.h>

/* Exported functions --------------------------------------------------------*/

void timeout_uart_init(uint32_t baudrate, uint32_t word_length, uint32_t parity, uint32_t flow_control);

void timeout_uart_deinit(void);

void timeout_uart_start(void);

void timeout_uart_set_baudrate(uint32_t baudrate);

/**
 * @brief Read data from UART, will only return data when there is enough available data or after a timeout
 * @param data pointer where the data will be put
 * @param size the size to read
 * @param timeout the timeout in tick
 * @return the effective read size
 */
uint16_t timeout_uart_read(uint8_t* data, uint16_t size, uint16_t timeout);

uint16_t timeout_uart_write(const uint8_t* data, uint16_t size);

uint16_t timeout_uart_get_write_available(void);

bool timeout_uart_is_busy(void);

#endif
