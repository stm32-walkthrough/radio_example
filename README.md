# radio_example
Example demonstrating the use of the [rm1s3_driver](https://gitlab.com/stm32-walkthrough/rm1s3_driver) by making a bridge between the UART and the radio.

## Installation
This example use a submodule so we need to do the following procedure
```
git clone --recurse-submodules https://gitlab.com/stm32-walkthrough/radio_example.git
```

## Hardware
The main branch is for NUCLEO-L452RE-P, there is another branch for the NUCLEO-L476RG.
- 2x NUCLEO-L452RE-P
- 2x RFShield rev.4

### RFShield solder bridge
The RFShield should have the default solder bridges with the following exception:
- SB0, SB1 and SB2 should be soldered

### RFShield jumper
We also need the following jumpers:
- one jumper for VCC and one for its LED (X1)
- one jumper for VCC_PA and one for its LED (X5)
- I5V on E5V (X11)
- TXD on EXT_TXD (X18)
- RXD on EXT_RXD (X23)

### Assembly
The RFShield should be plugged into the Nucleo board, a 868MHz antenna should be screw into the RFShield.
A USB cable should connect the Nucleo board with the PC.

## Programmation
One device should be programmed with the current code, the other one should redefine 'ADDRESS' to 'CLIENT_ADDRESS' in main.c.

## Usage
Open a terminal for each device with 9600 8N1. The data sent to a terminal will be received on the other.
